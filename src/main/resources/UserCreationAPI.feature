@API @Regression
  Feature: User creation
    Scenario Outline: As a client, I would like to create new users
      Given client is ready with required information
      Then verify that users created successfully "<name>", "<job>","<id>","<createdAt>"
      Examples:
      |name     | job       | id    |createdAt                              |
      |Yogesh  | Leader |  1     |2020-12-07T13:14:57.132Z  |

 @API @ScenarioGet
   Scenario: As a client, I would to fetch newly created users
     Given client is ready with required details
     Then Verify that newly created users fetched and those are correct