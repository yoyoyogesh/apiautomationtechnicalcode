package utility;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import org.junit.Assert;
import pojo.UsersPojo;
import com.fasterxml.jackson.databind.ObjectMapper;

public class APIAdapter {

    //Assert the Value
    public void assertEquals(String expectedValue, String actualValue)
    {
        Assert.assertEquals("Required assert failed", expectedValue, actualValue);
    }

    //Assert True
    public void assertTrue(int count)
    {
        Assert.assertTrue("count is not correct",count>=1);
    }

    //Create Payload
    public String createUsersPayload(String name, String job, String id, String createdAt) throws JsonProcessingException {
        UsersPojo usersPojo = new UsersPojo(name, job, id, createdAt);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(usersPojo);
    }

   }
