package pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UsersPojo {

    private String name;
    private String job;
    private String id;
    private String createdAt;

    public UsersPojo(String name, String job, String id, String createdAt) {
        this.name = name;
        this.job = job;
        this.id = id;
        this.createdAt = createdAt;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Job")
    public String getJob() {
        return job;
    }

    @JsonProperty("Id")
    public String getId() {
        return id;
    }

    @JsonProperty("CreatedAt")
    public String getCreatedAt() {
        return createdAt;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
