package StepDefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

import utility.APIAdapter;

import static io.restassured.RestAssured.given;

public class UsersCreationStepDef extends APIAdapter{
     //Note: Implement below points here
     //1. POJO Mechanism - Done
     //2. Payload via HashMap technique

    //Declare required details
   private RequestSpecification request;
   private Response response;
    APIAdapter apiAdapter = new APIAdapter();

    @Given("^client is ready with required information$")
    public void client_is_ready_with_required_information() throws Throwable {
        //Read token here if required

    }

    @Then("^verify that users created successfully \"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
    public void verify_that_users_created_successfully(String name, String job, String id, String CreatedAt) throws Throwable {

      //Declare Endpoint
        RestAssured.baseURI ="https://reqres.in/api";

        //Create payload via pojo
        String strUsersPayload = apiAdapter.createUsersPayload(name,job, id, CreatedAt);
        System.out.println("Payload via pojo: "+strUsersPayload);

        //pass payload in body
        request =given();
        request.header("Content-Type", "application/json").body(strUsersPayload);

        //Send request on POST call
        response = request.post("/users");
        //Print response
        System.out.println("Post response: "+ response.prettyPrint());

        //Verify StatusCode and response contents
       Assert.assertEquals("StatusCode is not correct",response.getStatusCode(), 201);

       String strName = response.jsonPath().get("Name");
       Assert.assertEquals("Name is not correct",name, strName);

    }

    @Given("^client is ready with required details$")
    public void client_is_ready_with_required_details() throws Throwable {
        //Read token here if required
        given().when().get("https://reqres.in/api/users/2").then().assertThat().statusCode(200);
    }

    @Then("^Verify that newly created users fetched and those are correct$")
    public void verify_that_newly_created_users_fetched_and_those_are_correct() throws Throwable {
        given().when().get("https://reqres.in/api/users?page=2").prettyPrint();

        }
}
