### QA Automation Engineer - GlobalKinetic

### API Automation with POSTMAN

### Pre-requisites
 IDE - IntelliJ
 Java version -16.0.1 
 Build tool - Maven build tool
 Test Design approach - BDD framework using cucumber tool where page object model design techniques is used to deal with web elements
                 Benefits of Framework:
				                1. Reusability
								2. Less cost required in order to design and development of framework.
								3. Maintenance is easy
								4. Code quality is achieved by using SonarLint and Sonarqube 
		
### Guidelines on how to execute your tests and how to view reporting 
Local Execution: Clone this project on your machine (windows preferrably ) and open in any IDE like Intellij/Eclipse. follow maven cycle and run test through TestRunner file.
Execution in pipeline: configure jenkins for maven project and execute test in pipeline by clicking Build now button.
Reports: test execution report can be found target >> cucumber-reports >> index.html

#### Task
Requirements: 
• Demonstrate functional API automation testing using Postman 
• Automate scenarios using different HTTP methods or verbs 

System Under test: 
Use the following public API: https://openweathermap.org/api 
(Note: You will need to sign-up for a free account to use these APIs) 

Criteria: 
Your submission should demonstrate: 
• Negative test scenarios 
• Use of assertions or tests  
• Use of parameterisation for any dynamic configs or paths 

Note: HTTP Post and GET operation has been automated by popular tool called Rest-assured and popular endpoint has been used for test.

#### How criteria achieved 
- Created feature file using Gherkin syntax
- Createx step definitions and link these to the feature file
- Written implementation which links both pieces up
- Createx TestRunner to run test
- followed coding standards
- Assert statements used for validations. 


